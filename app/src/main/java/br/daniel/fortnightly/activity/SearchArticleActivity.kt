package br.daniel.fortnightly.activity

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import br.daniel.fortnightly.R
import br.daniel.fortnightly.adapter.ArticlesAdapter
import br.daniel.fortnightly.adapter.ArticlesAdapter.AdapterType.SEARCH
import br.daniel.fortnightly.core.util.hideKeyboard
import br.daniel.fortnightly.core.util.observeEvent
import br.daniel.fortnightly.view.listener.ArticleClickHandler
import br.daniel.fortnightly.viewmodel.SearchArticleViewModel
import kotlinx.android.synthetic.main.activity_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchArticleActivity : AppCompatActivity(), ArticleClickHandler {

    private val viewModel by viewModel<SearchArticleViewModel>()
    private val articlesAdapter by lazy { ArticlesAdapter(SEARCH) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setupUI()
        subscribeUI()
    }

    private fun setupUI() {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        article_list.adapter = articlesAdapter
        articlesAdapter.articleClickListener = viewModel

        search.editText?.setOnEditorActionListener { _, actionId, _ ->

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.search(search.editText?.text?.toString() ?: "")
                search.editText.hideKeyboard()
                true
            } else {
                false
            }
        }
    }

    private fun subscribeUI() = viewModel.run {

        articles.observe(this@SearchArticleActivity) {
            articlesAdapter.refresh(it)
        }

        onArticleClicked.observeEvent(this@SearchArticleActivity) {
            this@SearchArticleActivity.handleArticleClicked(it)
        }
    }
}