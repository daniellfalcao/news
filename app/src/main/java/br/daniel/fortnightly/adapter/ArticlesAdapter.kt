package br.daniel.fortnightly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.daniel.fortnightly.R
import br.daniel.fortnightly.adapter.ArticlesAdapter.AdapterType.*
import br.daniel.fortnightly.adapter.ArticlesAdapter.ViewType.ARTICLE_LARGE
import br.daniel.fortnightly.adapter.ArticlesAdapter.ViewType.ARTICLE_SMALL
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.view.holder.ArticleBaseViewHolder
import br.daniel.fortnightly.view.holder.ArticleLargeViewHolder
import br.daniel.fortnightly.view.holder.ArticleSmallViewHolder
import br.daniel.fortnightly.view.listener.ArticleClickListener

class ArticlesAdapter(private val adapterType: AdapterType) :
    RecyclerView.Adapter<ArticleBaseViewHolder>() {

    enum class AdapterType { FIRST_PAGE, CATEGORY, SEARCH }
    enum class ViewType(val id: Int) { ARTICLE_SMALL(0), ARTICLE_LARGE(1) }

    private val items: MutableList<Article> = mutableListOf()
    var articleClickListener: ArticleClickListener? = null

    fun refresh(newItems: List<Article>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (adapterType) {
            FIRST_PAGE -> if (position == 0) ARTICLE_LARGE.id else ARTICLE_SMALL.id
            CATEGORY, SEARCH -> ARTICLE_SMALL.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleBaseViewHolder {

        return when (viewType) {
            ARTICLE_LARGE.id -> ArticleLargeViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.view_holder_category_large,
                        parent,
                        false
                    )
            )
            else -> ArticleSmallViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.view_holder_category_small,
                        parent,
                        false
                    )
            )
        }
    }

    override fun onBindViewHolder(holder: ArticleBaseViewHolder, position: Int) {
        holder.bind(items[position], articleClickListener)
    }
}