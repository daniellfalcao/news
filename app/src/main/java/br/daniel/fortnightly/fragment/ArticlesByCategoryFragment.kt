package br.daniel.fortnightly.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import br.daniel.fortnightly.R
import br.daniel.fortnightly.adapter.ArticlesAdapter
import br.daniel.fortnightly.adapter.ArticlesAdapter.AdapterType.CATEGORY
import br.daniel.fortnightly.core.state.ViewState
import br.daniel.fortnightly.core.util.observeEvent
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.view.listener.ArticleClickHandler
import br.daniel.fortnightly.viewmodel.ArticlesByCategoryViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_list_articles.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ArticlesByCategoryFragment : Fragment(), ArticleClickHandler {

    private val viewModel by viewModel<ArticlesByCategoryViewModel>()
    private val categoryAdapter by lazy { ArticlesAdapter(CATEGORY) }

    private lateinit var articleCategory: ArticleCategory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        articleCategory = this.arguments?.get(ArticleCategory.ARTICLE_CATEGORY) as ArticleCategory
        return inflater.inflate(R.layout.fragment_list_articles, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUI()
        subscribeUI()

        if (savedInstanceState == null) {
            viewModel.articleCategory = articleCategory
        }
    }

    private fun setupUI() {
        article_list.adapter = categoryAdapter
        categoryAdapter.articleClickListener = viewModel
        categoryAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                if (viewModel.viewState.value is ViewState.LoadingState
                    && categoryAdapter.itemCount != 0
                ) {
                    progress.visibility = View.GONE
                }
            }
        })
    }

    private fun subscribeUI() = viewModel.run {

        articles.observe(viewLifecycleOwner) { articles ->
            categoryAdapter.refresh(articles)
        }

        onArticleClicked.observeEvent(viewLifecycleOwner) { article ->
            requireContext().handleArticleClicked(article)
        }

        viewState.observe(viewLifecycleOwner) {

            progress.visibility = View.GONE
            empty_text.visibility = View.GONE
            error_text.visibility = View.GONE

            when (it) {
                ViewState.LoadingState -> {
                    println("adapter size = ${categoryAdapter.itemCount}")
                    progress.visibility =
                        if (categoryAdapter.itemCount == 0) View.VISIBLE else View.GONE
                }
                ViewState.EmptyState -> {
                    empty_text.visibility =
                        if (categoryAdapter.itemCount == 0) View.VISIBLE else View.GONE
                }
                is ViewState.ErrorState -> {
                    error_text.visibility =
                        if (categoryAdapter.itemCount == 0) View.VISIBLE else View.GONE
                    if (error_text.visibility == View.GONE) {
                        Snackbar.make(container, it.message(requireContext()), Snackbar.LENGTH_LONG)
                            .show()
                    } else {
                        error_text.text = it.message(requireContext())
                    }
                }
            }
        }
    }

}