package br.daniel.fortnightly.view.holder

import android.view.View
import br.daniel.fortnightly.R
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.view.listener.ArticleClickListener
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_holder_category_small.view.*

class ArticleSmallViewHolder(view: View) : ArticleBaseViewHolder(view) {

    override fun bind(article: Article, clickListener: ArticleClickListener?) {

        with(itemView) {

            setOnClickListener { clickListener?.onClickArticle(article) }

            if (article.category.isBlank()) {
                category_name.visibility = View.GONE
            } else {
                category_name.text = article.getFormattedCategory()
                category_name.visibility = View.VISIBLE
            }

            news_title.text = article.title
            Glide.with(this)
                .load(article.urlToImage)
                .placeholder(R.drawable.ic_article_placeholder)
                .into(news_thumbnail)
        }
    }
}