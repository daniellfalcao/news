package br.daniel.fortnightly.view.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.view.listener.ArticleClickListener

abstract class ArticleBaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(article: Article, clickListener: ArticleClickListener?)
}