package br.daniel.fortnightly.view.holder

import android.view.View
import br.daniel.fortnightly.R
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.view.listener.ArticleClickListener
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.view_holder_category_large.view.*

class ArticleLargeViewHolder(view: View) : ArticleBaseViewHolder(view) {

    override fun bind(article: Article, clickListener: ArticleClickListener?) {

        with(itemView) {
            setOnClickListener { clickListener?.onClickArticle(article) }
            category_name.text = article.getFormattedCategory()
            news_title.text = article.title
            Glide.with(this)
                .load(article.urlToImage)
                .placeholder(R.drawable.ic_article_placeholder)
                .into(news_thumbnail)
        }
    }
}