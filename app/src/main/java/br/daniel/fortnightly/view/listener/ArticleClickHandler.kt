package br.daniel.fortnightly.view.listener

import android.content.Context
import android.content.Intent
import br.daniel.fortnightly.activity.ArticleActivity
import br.daniel.fortnightly.data.ui.Article

interface ArticleClickHandler {

    fun Context.handleArticleClicked(article: Article) {
        Intent(this, ArticleActivity::class.java).also { intent ->
            intent.putExtra(Article.ARTICLE, article)
            startActivity(intent)
        }
    }
}