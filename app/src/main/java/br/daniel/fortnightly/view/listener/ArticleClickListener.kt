package br.daniel.fortnightly.view.listener

import androidx.lifecycle.MutableLiveData
import br.daniel.fortnightly.core.util.Event
import br.daniel.fortnightly.data.ui.Article

interface ArticleClickListener {

    val onArticleClicked: MutableLiveData<Event.Data<Article>>

    fun onClickArticle(article: Article) {
        onArticleClicked.value = Event.Data(article)
    }
}