package br.daniel.fortnightly.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.daniel.fortnightly.core.util.Event
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.domain.usecase.SearchArticleUseCase
import br.daniel.fortnightly.view.listener.ArticleClickListener
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.launch

class SearchArticleViewModel(
    private val searchArticle: SearchArticleUseCase
) : ViewModel(), ArticleClickListener {

    override val onArticleClicked = MutableLiveData<Event.Data<Article>>()

    private val _articles = MutableLiveData<List<Article>>()
    val articles = _articles as LiveData<List<Article>>

    private var searchJob: Job? = null

    fun search(query: String) = viewModelScope.launch {
        searchJob?.cancelAndJoin()
        searchJob = viewModelScope.launch {

            searchArticle(SearchArticleUseCase.Params(query))
                .onStarted {

                }.onSuccess {
                    _articles.value = it
                }.onFailure {

                }.onFinish {

                }.execute()
        }
    }
}