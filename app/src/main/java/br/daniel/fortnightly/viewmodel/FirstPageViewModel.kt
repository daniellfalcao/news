package br.daniel.fortnightly.viewmodel

import androidx.lifecycle.*
import br.daniel.fortnightly.core.util.Event
import br.daniel.fortnightly.core.util.SingleMediatorLiveData
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.domain.usecase.GetArticlesOfFirstPageUseCase
import br.daniel.fortnightly.view.listener.ArticleClickListener
import kotlinx.coroutines.launch

class FirstPageViewModel(
    private val getArticlesFirstPage: GetArticlesOfFirstPageUseCase
): ViewModel(), ArticleClickListener {

    override val onArticleClicked = MutableLiveData<Event.Data<Article>>()

    private val _articles = SingleMediatorLiveData<List<Article>>().apply {
        viewModelScope.launch { this@apply.emit(getArticlesFirstPage.liveResult().asLiveData()) }
    }
    val articles = _articles as LiveData<List<Article>>

    fun loadArticles() = viewModelScope.launch {

        getArticlesFirstPage()
            .onStarted {

            }.onSuccess {

            }.onFailure {

            }.onFinish {

            }.execute()
    }
}