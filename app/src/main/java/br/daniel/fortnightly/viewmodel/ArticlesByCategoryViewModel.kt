package br.daniel.fortnightly.viewmodel

import androidx.lifecycle.*
import br.daniel.fortnightly.core.state.ViewState
import br.daniel.fortnightly.core.util.Event
import br.daniel.fortnightly.core.util.SingleMediatorLiveData
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.domain.usecase.GetArticlesOfCategoryUseCase
import br.daniel.fortnightly.view.listener.ArticleClickListener
import kotlinx.coroutines.launch

class ArticlesByCategoryViewModel(
    private val getArticlesOfCategory: GetArticlesOfCategoryUseCase
) : ViewModel(), ArticleClickListener {

    override val onArticleClicked = MutableLiveData<Event.Data<Article>>()

    private val _articles = SingleMediatorLiveData<List<Article>>()
    val articles = _articles as LiveData<List<Article>>

    private val _viewState = MutableLiveData<ViewState>()
    val viewState = _viewState as LiveData<ViewState>

    var articleCategory: ArticleCategory? = null
        set(value) {
            if (value != null) {
                field = value
                viewModelScope.launch {
                    _articles.emit(getArticlesOfCategory.liveResult(
                        GetArticlesOfCategoryUseCase.Params(value)
                    ).asLiveData())
                    loadArticlesOfCategory(value)
                }
            }
        }

    private fun loadArticlesOfCategory(category: ArticleCategory) = viewModelScope.launch {

        getArticlesOfCategory(GetArticlesOfCategoryUseCase.Params(category))
            .onStarted {
                _viewState.value = ViewState.LoadingState
            }.onSuccess {
                _viewState.value = if (it.isEmpty()) ViewState.EmptyState else ViewState.NoState
            }.onFailure {
                _viewState.value = ViewState.ErrorState(it.errorMessage)
            }.execute()
    }

}