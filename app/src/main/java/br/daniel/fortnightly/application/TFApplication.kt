package br.daniel.fortnightly.application

import android.app.Application
import br.daniel.fortnightly.data._config.TFDatabase
import br.daniel.fortnightly.domain.repository.ArticleLocalRepository
import br.daniel.fortnightly.domain.repository.ArticleRemoteRepository
import br.daniel.fortnightly.domain.repository.ArticleRepository
import br.daniel.fortnightly.domain.usecase.GetArticlesOfCategoryUseCase
import br.daniel.fortnightly.domain.usecase.GetArticlesOfFirstPageUseCase
import br.daniel.fortnightly.domain.usecase.SearchArticleUseCase
import br.daniel.fortnightly.viewmodel.ArticleViewModel
import br.daniel.fortnightly.viewmodel.ArticlesByCategoryViewModel
import br.daniel.fortnightly.viewmodel.FirstPageViewModel
import br.daniel.fortnightly.viewmodel.SearchArticleViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class TFApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {

        startKoin {

            androidContext(this@TFApplication)

            val daoModule = module {
                single { TFDatabase.getInstance(get()) }
                single { get<TFDatabase>().articleDAO }
                single { get<TFDatabase>().transactionDAO }
            }

            val repositoryModule = module {
                factory { ArticleRemoteRepository() }
                factory { ArticleLocalRepository(articleDAO = get()) }
                factory { ArticleRepository(articleLocalRepository = get(), articleRemoteRepository = get()) }
            }

            val useCaseModule = module {
                factory { GetArticlesOfCategoryUseCase(articleRepository = get()) }
                factory { GetArticlesOfFirstPageUseCase(articleRepository = get()) }
                factory { SearchArticleUseCase(articleRepository = get()) }
            }

            val viewModelModule = module {
                factory { ArticlesByCategoryViewModel(getArticlesOfCategory = get()) }
                factory { ArticleViewModel() }
                factory { FirstPageViewModel(getArticlesFirstPage = get()) }
                factory { SearchArticleViewModel(searchArticle = get()) }
            }

            modules(listOf(daoModule, repositoryModule, useCaseModule, viewModelModule))
        }
    }

}