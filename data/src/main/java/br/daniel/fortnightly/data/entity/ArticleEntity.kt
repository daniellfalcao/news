package br.daniel.fortnightly.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

enum class ArticleCategory(val categoryName: String, val optionName: String) {
    BUSINESS("business", "Negócio"),
    ENTERTAINMENT("entertainment", "Entretenimento"),
    GENERAL("general", "Geral"),
    HEALTH("health", "Saúde"),
    SCIENCE("science", "Ciência"),
    SPORTS("sports", "Esportes"),
    TECHNOLOGY("technology", "Tecnologia");

    companion object {

        const val ARTICLE_CATEGORY = "article_category"

        fun getByCategoryName(categoryName: String): ArticleCategory {
            return when (categoryName) {
                BUSINESS.categoryName -> BUSINESS
                ENTERTAINMENT.categoryName -> ENTERTAINMENT
                GENERAL.categoryName -> GENERAL
                HEALTH.categoryName -> HEALTH
                SCIENCE.categoryName -> SCIENCE
                SPORTS.categoryName -> SPORTS
                else -> TECHNOLOGY
            }
        }

        fun getByOptionName(optionName: String): ArticleCategory {
            return when (optionName) {
                BUSINESS.optionName -> BUSINESS
                ENTERTAINMENT.optionName -> ENTERTAINMENT
                GENERAL.optionName -> GENERAL
                HEALTH.optionName -> HEALTH
                SCIENCE.optionName -> SCIENCE
                SPORTS.optionName -> SPORTS
                else -> TECHNOLOGY
            }
        }
    }

}

@Entity(tableName = "article")
data class ArticleEntity(
    var author: String = "",
    var title: String = "",
    var description: String = "",
    var url: String? = null,
    var urlToImage: String? = null,
    var publishedAt: String = "",
    var content: String = "",
    var category: String = "",
    @PrimaryKey(autoGenerate = true) var id: Int = 0
)