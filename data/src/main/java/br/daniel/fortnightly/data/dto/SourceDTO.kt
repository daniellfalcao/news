package br.daniel.fortnightly.data.dto

data class SourceDTO(
    var id: String? = null,
    var name: String? = null
)