package br.daniel.fortnightly.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Query
import br.daniel.fortnightly.data._config.dao.DAO
import br.daniel.fortnightly.data.entity.ArticleEntity
import kotlinx.coroutines.flow.Flow

@Dao
abstract class ArticleDAO : DAO<ArticleEntity>() {

    @Query("SELECT * FROM article")
    abstract fun getAll(): Flow<List<ArticleEntity>>

    @Query("SELECT * FROM article WHERE category = :category")
    abstract fun getByCategory(category: String): Flow<List<ArticleEntity>>

    @Query("DELETE FROM article WHERE category = :category")
    abstract suspend fun deleteByCategory(category: String)

}