package br.daniel.fortnightly.data.dto

data class ArticleResponseDTO(
    var articles: List<ArticleDTO>? = null
)