package br.daniel.fortnightly.data

import br.daniel.fortnightly.data.dto.ArticleDTO
import br.daniel.fortnightly.data.entity.ArticleEntity
import br.daniel.fortnightly.data.ui.Article

fun ArticleDTO.toEntity() = ArticleEntity(
    author = this.author ?: "",
    title = this.title ?: "",
    description = this.description ?: "",
    url = this.url,
    urlToImage = this.urlToImage,
    publishedAt = this.publishedAt ?: "",
    content = this.content ?: "",
    category = this.category ?: ""
)

fun List<ArticleDTO>.toEntity(): List<ArticleEntity> {
    return this.map { it.toEntity() }
}

fun ArticleDTO.toUI() = Article(
    author = this.author ?: "",
    title = this.title ?: "",
    description = this.description ?: "",
    url = this.url,
    urlToImage = this.urlToImage,
    publishedAt = this.publishedAt ?: "",
    content = this.content ?: "",
    category = this.category ?: ""
)

fun List<ArticleDTO>.toUI(): List<Article> {
    return this.map { it.toUI() }
}

fun ArticleEntity.toUI() = Article(
    author = this.author,
    title = this.title,
    description = this.description,
    url = this.url,
    urlToImage = this.urlToImage,
    publishedAt = this.publishedAt,
    content = this.content,
    category = this.category
)

@JvmName("articleEntityToUI")
fun List<ArticleEntity>.toUI(): List<Article> {
    return this.map { it.toUI() }
}