package br.daniel.fortnightly.core.state

import br.daniel.fortnightly.core.util.StringWrapper

sealed class ViewState {
    object NoState : ViewState()
    object LoadingState : ViewState()
    object EmptyState : ViewState()
    class ErrorState(val message: StringWrapper) : ViewState()
}