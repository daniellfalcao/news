package br.daniel.fortnightly.domain.repository

import br.daniel.fortnightly.core.util.TFResult
import br.daniel.fortnightly.core.util.onSuccess
import br.daniel.fortnightly.data.dto.ArticleDTO
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.data.entity.ArticleEntity
import br.daniel.fortnightly.domain._config.repository.TFRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class ArticleRepository (
    private val articleLocalRepository: ArticleLocalRepository,
    private val articleRemoteRepository: ArticleRemoteRepository
): TFRepository() {

    fun getFirstPage(): Flow<List<ArticleEntity>> {

        fun mapArticlesByCategory(articles: List<ArticleEntity>): List<ArticleEntity> {
            val articlesByCategory = mutableListOf<ArticleEntity>()
            articles.groupBy { it.category }.toMutableMap().forEach {
                try {
                    articlesByCategory.add(it.value[0])
                } catch (error: Exception) {
                }
            }
            return articlesByCategory
        }

        return articleLocalRepository.getAllArticles().map { articles ->
            mapArticlesByCategory(articles)
        }
    }

    fun getArticlesOfCategory(category: ArticleCategory): Flow<List<ArticleEntity>> {
        return articleLocalRepository.getArticlesOfCategory(category)
    }

    suspend fun fetchArticlesOfCategory(category: ArticleCategory): TFResult<List<ArticleDTO>> {
        return articleRemoteRepository.fetchTopHeadlines(category).onSuccess { articles ->
            articleLocalRepository.saveArticlesOfCategory(articles, category)
        }
    }

    suspend fun fetchArticlesOfQuery(query: String): TFResult<List<ArticleDTO>> {
        return articleRemoteRepository.fetchEverything(query)
    }
}