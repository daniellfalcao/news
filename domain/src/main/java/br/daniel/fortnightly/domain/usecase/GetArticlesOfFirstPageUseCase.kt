package br.daniel.fortnightly.domain.usecase

import br.daniel.fortnightly.core.exception.TFException
import br.daniel.fortnightly.core.util.TFResult
import br.daniel.fortnightly.core.util.throwOnFailure
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.data.toUI
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.domain._config.usecase.TFUseCase
import br.daniel.fortnightly.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.*

class GetArticlesOfFirstPageUseCase(
    private val articleRepository: ArticleRepository
) : TFUseCase.Completable<Unit, Any, Flow<List<Article>>>() {

    override suspend fun liveResult(params: Unit?) = flow {
        emitAll(articleRepository.getFirstPage().map { it.toUI() })
    }

    override suspend fun execute(params: Unit?): TFResult<Any> {

        return try {
            articleRepository.fetchArticlesOfCategory(ArticleCategory.BUSINESS).throwOnFailure()
            articleRepository.fetchArticlesOfCategory(ArticleCategory.ENTERTAINMENT).throwOnFailure()
            articleRepository.fetchArticlesOfCategory(ArticleCategory.GENERAL).throwOnFailure()
            articleRepository.fetchArticlesOfCategory(ArticleCategory.HEALTH).throwOnFailure()
            articleRepository.fetchArticlesOfCategory(ArticleCategory.SCIENCE).throwOnFailure()
            articleRepository.fetchArticlesOfCategory(ArticleCategory.SPORTS).throwOnFailure()
            articleRepository.fetchArticlesOfCategory(ArticleCategory.TECHNOLOGY).throwOnFailure()
            TFResult.success("OK")
        } catch (error: TFException) {
            TFResult.failure(error)
        }
    }
}