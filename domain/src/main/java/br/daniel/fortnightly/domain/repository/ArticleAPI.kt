package br.daniel.fortnightly.domain.repository

import br.daniel.fortnightly.data.dto.ArticleResponseDTO
import br.daniel.fortnightly.domain._config.repository.TFRepository.Companion.API_KEY
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ArticleAPI {

    @GET("everything")
    suspend fun fetchEverything(
        @Query("q") query: String,
        @Query("language") country: String = "pt",
        @Header(API_KEY) token: String = "18aba0fa00a34673b02e057a16dbce0e"
    ): ArticleResponseDTO

    @GET("top-headlines")
    suspend fun fetchTopHeadlines(
        @Query("category") category: String,
        @Query("country") country: String = "br",
        @Header(API_KEY) token: String = "18aba0fa00a34673b02e057a16dbce0e"
    ): ArticleResponseDTO
}