package br.daniel.fortnightly.domain.repository

import br.daniel.fortnightly.data.dao.ArticleDAO
import br.daniel.fortnightly.data.dto.ArticleDTO
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.data.entity.ArticleEntity
import br.daniel.fortnightly.data.toEntity
import br.daniel.fortnightly.domain._config.repository.TFRepository
import kotlinx.coroutines.flow.Flow

class ArticleLocalRepository(
    private val articleDAO: ArticleDAO
) : TFRepository.Local() {

    fun getAllArticles(): Flow<List<ArticleEntity>> {
        return articleDAO.getAll()
    }

    fun getArticlesOfCategory(category: ArticleCategory): Flow<List<ArticleEntity>> {
        return articleDAO.getByCategory(category.categoryName)
    }

    suspend fun saveArticlesOfCategory(articles: List<ArticleDTO>, category: ArticleCategory) = executeTransaction {
        deleteArticlesOfCategory(category)
        saveArticles(articles)
    }

    suspend fun saveArticles(articles: List<ArticleDTO>) {
        articleDAO.insertOrUpdate(articles.toEntity())
    }

    suspend fun deleteArticlesOfCategory(category: ArticleCategory) {
        articleDAO.deleteByCategory(category.categoryName)
    }

}