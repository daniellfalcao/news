package br.daniel.fortnightly.domain.repository

import br.daniel.fortnightly.core.util.TFResult
import br.daniel.fortnightly.core.util.map
import br.daniel.fortnightly.core.util.onSuccess
import br.daniel.fortnightly.data.dto.ArticleDTO
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.domain._config.repository.TFRepository

class ArticleRemoteRepository : TFRepository.Remote() {

    private val api by retrofit<ArticleAPI>()

    suspend fun fetchEverything(query: String): TFResult<List<ArticleDTO>> {
        return executeRequest(api) { fetchEverything(query) }
            .map { it.articles ?: emptyList() }
    }

    suspend fun fetchTopHeadlines(category: ArticleCategory): TFResult<List<ArticleDTO>> {
        return executeRequest(api) { fetchTopHeadlines(category.categoryName) }
            .onSuccess { response ->
                response.articles?.forEach { it.category = category.categoryName }
            }.map { it.articles ?: emptyList() }
    }
}