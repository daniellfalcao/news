package br.daniel.fortnightly.domain.usecase

import br.daniel.fortnightly.core.exception.TFException
import br.daniel.fortnightly.core.util.TFResult
import br.daniel.fortnightly.core.util.map
import br.daniel.fortnightly.data.toUI
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.domain._config.usecase.TFUseCase
import br.daniel.fortnightly.domain.repository.ArticleRepository
import br.daniel.fortnightly.domain.usecase.SearchArticleUseCase.Params

class SearchArticleUseCase(
    private val articleRepository: ArticleRepository
) : TFUseCase.Completable<Params, List<Article>, Unit>() {

    data class Params(val query: String)

    override suspend fun execute(params: Params?): TFResult<List<Article>> {
        if (params == null) return TFResult.failure(TFException())
        return articleRepository.fetchArticlesOfQuery(params.query).map { it.toUI() }
    }

}