package br.daniel.fortnightly.domain.usecase

import br.daniel.fortnightly.core.exception.TFException
import br.daniel.fortnightly.core.util.TFResult
import br.daniel.fortnightly.core.util.map
import br.daniel.fortnightly.data.entity.ArticleCategory
import br.daniel.fortnightly.data.toEntity
import br.daniel.fortnightly.data.toUI
import br.daniel.fortnightly.data.ui.Article
import br.daniel.fortnightly.domain._config.usecase.TFUseCase
import br.daniel.fortnightly.domain.repository.ArticleRepository
import br.daniel.fortnightly.domain.usecase.GetArticlesOfCategoryUseCase.Params
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

class GetArticlesOfCategoryUseCase(
    private val articleRepository: ArticleRepository
) : TFUseCase.Completable<Params, List<Article>, Flow<List<Article>>>() {

    data class Params(val category: ArticleCategory)

    override suspend fun liveResult(params: Params?) = flow {
        if (params == null) {
            emit(emptyList())
        } else {
            emitAll(articleRepository.getArticlesOfCategory(params.category).map { it.toUI() })
        }
    }

    override suspend fun execute(params: Params?): TFResult<List<Article>> {

        if (params == null) return TFResult.failure(TFException())

        return articleRepository.fetchArticlesOfCategory(params.category)
            .map { it.toEntity().toUI() }
    }
}